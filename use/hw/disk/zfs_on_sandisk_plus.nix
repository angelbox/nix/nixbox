{ config, pkgs, ... }:

{
  imports = [ ./zfs_on_ssd.nix ];

  services.zfs.trim.enable = false; # avoid corruption on SanDisk Plus SSD
}
