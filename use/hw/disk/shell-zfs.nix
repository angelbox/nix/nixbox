# Shell aliases etc

{ config, ... }:

{
  environment.interactiveShellInit = ''
    alias zfs='sudo zfs'
    alias zs='zpool status'
    alias zh='zpool history'
    alias zl='zfs list'
  '';
}
