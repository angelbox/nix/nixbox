{ config, pkgs, ... }:

{
  imports = [ ./zfs.nix ];

  # http://toxicfrog.github.io/automounting-zfs-on-nixos/
  boot.postBootCommands = ''
 
        # Mount all the other zfs volumes automatically
        # to save having to manage hardware-configuration.nix

	echo "=== STARTING ZPOOL IMPORT ==="

	# Clean up borg-repo because the activation script creates it
	# KPH - not using borg so commenting out this line for now
	# ${pkgs.findutils}/bin/find /backup/borg-repo -maxdepth 1 -type d -empty -delete

	${pkgs.zfs}/bin/zpool import -a -f -N -d /dev/disk/by-path
	${pkgs.zfs}/bin/zpool status
 	${pkgs.zfs}/bin/zfs mount -a

	echo "=== ZPOOL IMPORT COMPLETE ==="
 '';
}
