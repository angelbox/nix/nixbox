{ config, pkgs, ... }:

{
  imports = [ 
    ./shell-zfs.nix
    ../networking/auto_set-hostId.nix
  ];

  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.devNodes = "/dev/disk/by-path";
}
