{ config, pkgs, ... }:

{
  imports = [ ./zfs_automount.nix ];

  services.zfs.autoScrub.enable = true;
}
