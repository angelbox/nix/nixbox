{ config, pkgs, ... }:
{
  # https://wiki.gentoo.org/wiki/Efibootmgr
  environment.systemPackages = [ pkgs.efibootmgr ];  
}
