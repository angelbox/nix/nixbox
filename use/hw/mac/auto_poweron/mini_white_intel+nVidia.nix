# https://blog.dhampir.no/content/linux-on-mac-mini-power-on-after-power-loss
# run on every reboot

{ config, pkgs, ... }: 
{ 
  config.systemd.services.enable-autorestart = {
    script   = "/run/current-system/sw/bin/setpci -s 00:03.0 0x7b.b=0x19";
    wantedBy = [ "default.target" ];
    after    = [ "default.target" ]; 
  };
      
  # https://www.linuxfromscratch.org/blfs/view/svn/general/pciutils.html
  config.environment.systemPackages = [ pkgs.pciutils ];
}