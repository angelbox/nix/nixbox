{ config, lib, ... }:

{
  # Enable sound.
  sound.enable = lib.mkDefault true;
  hardware.pulseaudio.enable = lib.mkDefault true;
}