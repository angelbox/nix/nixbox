# https://haydenjames.io/linux-performance-almost-always-add-swap-part2-zram/

{ config, lib, ... }:

{
  zramSwap.enable = lib.mkDefault true;
}
