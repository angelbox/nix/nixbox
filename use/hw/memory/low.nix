# low memory settings
# https://haydenjames.io/linux-performance-almost-always-add-swap-part2-zram/

{ config, ... }:

{
  boot.kernel.sysctl = {
    "vm.vfs_cache_pressure"     = 500;
    "vm.swappiness"             = 100;
    "vm.dirty_background_ratio" = 1;
    "vm.dirty_ratio"            = 50;
  };
}