#. ## This Server Never Sleeps
#. disuade gnome gui from putting us to sleep (if present)
#. https://discourse.nixos.org/t/stop-pc-from-sleep/5757
#< p

{ ... }: {
  systemd.targets.sleep.enable        = false;
  systemd.targets.suspend.enable      = false;
  systemd.targets.hibernate.enable    = false;
  systemd.targets.hybrid-sleep.enable = false;
}

