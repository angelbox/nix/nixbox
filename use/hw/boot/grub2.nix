# The machine is happy to boot using grub2 in EFI
# by happy, on a mac that basically means using refindit
# since the built in firmware boot manager is definitely broken

{ config, pkgs, lib, ... }:

let

  useEFI = config.boot.loader.grub.efiSupport;

in

{
  boot.loader.timeout = 3;
 
  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;

  # boot.loader.grub.efiInstallAsRemovable = true;
  boot.loader.efi.efiSysMountPoint = lib.mkDefault "/boot";

  # Define on which hard drive you want to install Grub.
  # or "nodev" for efi only
  
  boot.loader.grub.device = if useEFI then "nodev" else "/dev/sda";

  boot.loader.grub.configurationLimit = 6;
    
  boot.cleanTmpDir = true;

}
