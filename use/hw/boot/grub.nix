{ config, pkgs, lib, ... }:

{
  boot.loader.grub = {
    enable = true;
    # version = 2;
    efiSupport = true;
    efiInstallAsRemovable = true;
    device = "nodev";
    configurationLimit = 10;
  };

  boot.loader.timeout = 5;

  boot.cleanTmpDir = true;
}