# It´s a it experimental, but we get away without using refindit

{ config, pkgs, lib, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.timeout = 3;
  
  # grub2 boot loader options
  boot.loader.grub.efiSupport = true;

  boot.cleanTmpDir = true;
}

