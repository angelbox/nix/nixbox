# Using router dhcp

{ config, lib, ... }:

{
  imports = [ ./dhcp.nix ];

  # Disable predictable interface names 
  # Use the traditional eth0 so as to be 
  # common on all instances
  
  # boot.kernelParams = [ "net.ifnames=0" ];
  networking.usePredictableInterfaceNames = lib.mkDefault false;

  networking.interfaces.eth0.useDHCP = true;
 
}

