{ config, lib, ... }:

let 
  interface = config.networking.primary_wireless;
in
{
  # Enables wireless support via wpa_supplicant.
  networking.wireless.enable                   = lib.mkDefault true;  
  networking.interfaces."${interface}".useDHCP = lib.mkForce config.networking.wireless.enable;

  networking.networkmanager = {
    enable = lib.mkDefault false; # default
    # if enabled don't manage wireless
    unmanaged = [ "${interface}" ];
  };
}