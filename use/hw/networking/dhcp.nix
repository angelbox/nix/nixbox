# Using dhcp

{ config, lib, ... }:

{
  networking.useDHCP = lib.mkDefault false; # now invoked per interface

  # Fix for when network-setup reports invalid nextHop
  # it seems that dhcpd is not quite ready

  systemd.services.network-setup.after = [ "dhcpcd.service" ];
}

