# Enable the OpenSSH daemon.

{ config, lib, ... }:

{
  services.openssh.enable = lib.mkDefault true;

  # Root login is allowed with a valid ssh key
  # This is only needed in case that user accounts are broken
  # at some sensible point this can be switched to "no"

  services.openssh.permitRootLogin = lib.mkDefault "prohibit-password";
  services.openssh.startWhenNeeded = true;
}



	
