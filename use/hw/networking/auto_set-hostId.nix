{ pkgs, lib, ... }: 

{
  networking.hostId = builtins.substring 0 8 (builtins.readFile "/etc/machine-id");
}
