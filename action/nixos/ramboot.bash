#!/usr/bin/env bash

# Alchemy - Non-Destructive
# Tested on nixos-22.05, ubuntu-22.04

set -a +u

[[ -f ~/.bashrc ]] && source ~/.bashrc;

# set SSHKEY_PUB to your key and run as root
SSHKEY_PUB="${SSHKEY_PUB:-}"
[[ -z "$SSHKEY_PUB" ]] && SSHKEY_PUB="${SSH_ORIGINAL_COMMAND:-}"
[[ -z "$SSHKEY_PUB" ]] \
        && [[ -f /etc/ssh/authorized_keys.d/root ]] \
        && SSHKEY_PUB=$(cat /etc/ssh/authorized_keys.d/root)
[[ -z "$SSHKEY_PUB" ]] \
        && [[ -f ~/.ssh/authorized_keys ]] \
        && SSHKEY_PUB=$(head -1  ~/.ssh/authorized_keys)

mkdir -p /root/ramboot
cd /root/ramboot

build=$(which nix-build || echo "/nix/var/nix/profiles/default/bin/nix-build" ) 
$build '<nixpkgs/nixos>' -A config.system.build.kexec_tarball \
        -I nixos-config=/tmp/action/kexec/configuration.nix

tar -xf result/tarball/nixos-system-*.tar.xz
rm -rf result

#sudo ./kexec_nixos