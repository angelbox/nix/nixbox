#!/usr/bin/env bash

# run as sudo
# Stages a nixos takeover of a cloud compute instance
# Does not change the disk layout.

here="${BASH_SOURCE[0]%/*}"
set -xa

if [[ -z "${stateVersion}" ]]; then
    echo
    echo "+action/nixos/infect requires stateVersion to be explicitly supplied:"
    echo "#> ssh <box.surname>+action/nixos/infect:stateVersion=22.11:CONFIRM=yes"
    echo
fi

NIX_CHANNEL="${NIX_CHANNEL:-nixos-$stateVersion}"
NO_REBOOT=true

[[ "${CONFIRM:-no}" != "yes" ]] && echo "This is destructive, to proceed $1:CONFIRM=yes" && exit 1

curl https://raw.githubusercontent.com/elitak/nixos-infect/master/nixos-infect | bash -x

mv /etc/nixos/configuration.nix /etc/nixos/configuration.orig.nix
ln -s configuration.orig.nix /etc/nixos/configuration.nix

rm -rf /old-root

"${REBOOT:-true}" && reboot

:
