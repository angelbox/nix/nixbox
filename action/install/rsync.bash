# rsync default

here="${BASH_SOURCE[0]%/*}"
me="${me:-${BASH_SOURCE[0]}}"

if which apt
then
    sudo apt update
    sudo apt install rsync
fi