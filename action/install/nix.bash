#!/usr/bin/env bash

setenforce 0 || true

curl -Lv https://nixos.org/nix/install -o /tmp/nix-install.sh
sh /tmp/nix-install.sh --daemon
