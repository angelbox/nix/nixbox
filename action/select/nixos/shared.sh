# the simplest template processor ever
function render_template()
{
    eval "echo \"$(cat $1)\""
}

function deploy_configuration()
{
    me="$1"
    here="${me%/*}"
    this_configuration="${me%.bash}"
    this_configuration="${this_configuration##*/}"
    this_option="${this_configuration#configuration.}"

    requested_configuration="${requested_configuration:-$this_configuration}"
    nixos="${nixos:-/etc/nixos}"

    if [[ -d "${here}/${this_configuration}/nixos" ]]
    then
        rsync -a --delete "${here}/${this_configuration}/nixos/"* "$nixos"
    fi
     
    render_template "${here}/${this_configuration}/${this_configuration}.nix.tmpl" \
        | tee "$nixos/${this_configuration}.nix"

    # inherit from
    source "$here/configuration.${super}.bash"

    echo "Configuration symlink changed, ${name_surname}+unlock required."
}