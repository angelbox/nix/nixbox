{ pkgs, ... }:
{
  nixpkgs.overlays = [
    (final: prev: 
        {
            lib = (prev.lib or {}) // {

                padString = n: s: s + (prev.lib.substring 0 (n - prev.lib.stringLength s)   
                "                                                                  ");

                padStringLeft = n: s: prev.lib.fixedWidthString n " " s;
            };
        } )
   ];
}

