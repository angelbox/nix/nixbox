{ pkgs ? import <nixpkgs> {}, nixt  }:

with pkgs.lib;

let
    
    dns.hosts."intranet" = {
        description = "Intranet Server";
        nickname = "intranet";    
        ip4 = "10.10.10.2";
        hostname = "matthew";
        domainName = "matthew.flat";
        ownTLD = "matt";
    };
    dns.hosts."extranet" = {
        description = "External Server";
        comment = "External Server";
        nickname = "extranet";    
        hostname = "luke";
        domainName = "luke.flat";
        ownTLD = "luke";
    };

       # given a host..domain record (domainRec) return ip4/ip6 address
    getIP4 = record: record.ip4 or "";
    getIP6 = record: record.ip6 or "";

    # entry even if no ip found
    dnsEntryAll = getter: key: records: 
        let
            record = getAttr key records;
            ip = getter record;
            hostName = if record?hostname then record.hostname else "";
             domainName = if record?domainName then record.domainName else "";
            nickname = if record?nickname then record.nickname else "";
            comment = if record?comment then record.comment else key;
        in
            if (getter record != "") 
                then "${ip} ${domainName} ${hostName} ${nickname}  # ${comment}"
                else "#<no ip> ${domainName} ${hostName} ${nickname}  # ${comment}";    

    # entry only if ip given
    dnsEntry = getter: key: records: 
        let
            record = getAttr key records;
            ip = getter record;
           hostName = if record?hostname then record.hostname else "";
            domainName = if record?domainName then record.domainName else "";
            nickname = if record?nickname then record.nickname else "";
            comment = if record?comment then record.comment else key;
        in
            if (getter record != "") 
            then "${ip} ${domainName} ${hostName} ${nickname}  # ${key}"
            else "";
   

    dnsEntries = data: fmt: forEach (attrNames data) (k: (fmt k data) );

    dnsHosts = data: fmt: concatStringsSep "\n" (remove "" (dnsEntries data fmt));
   
in

nixt.mkSuite "/etc/hosts-2" {
   
    getIP4 = ("10.10.10.2" == getIP4 dns.hosts.intranet);

    dnsEntry = ("10.10.10.2 matthew.flat matthew intranet  # intranet" 
                    == (dnsEntry getIP4 "intranet" dns.hosts));

    dnsEntryAll = ("#<no ip> luke.flat luke extranet  # External Server"
                    == dnsEntryAll getIP4 "extranet" dns.hosts);

    dnsEntries = ((dnsEntries dns.hosts (dnsEntry getIP4)) 
                    == ["" "10.10.10.2 matthew.flat matthew intranet  # intranet"]);

    dnsHostsAll = ("#<no ip> luke.flat luke extranet  # External Server\n10.10.10.2 matthew.flat matthew intranet  # intranet" 
                    == dnsHosts dns.hosts (dnsEntryAll getIP4));

    dnsHosts = ("10.10.10.2 matthew.flat matthew intranet  # intranet" 
                    == dnsHosts dns.hosts (dnsEntry getIP4));
/*
        set2ValuesInner = [1] == set2ValuesInner { a = 1; };
        set2ValuesInner2 = [1 2] == set2ValuesInner { a = 1; b = 2;};
        
        
        "debug" = {
            hosts = hosts "NIXOS-CACHE" config.dns.keys.nixos-cache;
            dnsEntries = dnsEntries;
            dnsHosts = dnsHosts;
            dnsHostsFile = forEach dnsHosts (each: (forEach (attrNames each) (key: getAttr key each)  ) );
        };
        # userList = (map (key: getAttr key users) (attrNames users));
    */
}
