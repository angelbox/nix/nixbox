# This is our DNS server

{ config, lib, ... }:

with lib;

let
  eligible =  filterAttrs (key: hostRecord: hostRecord.ownTLD != "") config.dns.hosts;
  
  fmt = r: "address=/.${r.ownTLD}/${r.ip4}";

  wildcards = concatStringsSep "\n" (forEach (attrNames eligible) (key: fmt (getAttr key eligible) ));

  domain = config.dns.domain;

in

mkIf config.services.dnsmasq.enable
 
{
  services.dnsmasq = {
      extraConfig = ''
            # local domain
            domain=${domain}
            local=/${domain}/
            
            # internal wildcard domains
            ${wildcards}
      '';
  };
}

