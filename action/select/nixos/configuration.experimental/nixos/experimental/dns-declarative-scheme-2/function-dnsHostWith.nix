{ config, pkgs, lib, ... }:
with lib;
{
  nixpkgs.overlays = [
    (final: prev: 
        {
            lib = (prev.lib or {}) // {
                dnsHostWithNickname = nick: (head (attrValues (filterAttrs (k: v: v.nickname == nick) config.dns.hosts)));
            };
        } )
   ];
}

