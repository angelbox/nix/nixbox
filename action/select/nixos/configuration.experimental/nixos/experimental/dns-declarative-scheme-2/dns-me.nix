{ config, ... }:

let 
    me = config.networking.hostName; # 'box' host name used as the key to dns model records
in
{
    dns.me = me;
    dns.my = builtins.getAttr me config.dns.hosts;
}
