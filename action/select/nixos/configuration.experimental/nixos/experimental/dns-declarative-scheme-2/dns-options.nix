#. ## Nixnet network domain map and host properties (scheme 2)
#.
#. dns.hosts."intranet" = {
#.    description = "Intranet Server"
#.    nickname = "intranet";    
#.    ip4 = 10.10.10.2;
#.    hostname = "matthew";
#.    domainName = "matthew.flat";
#.    ownTLD = "matt";
#. }
#.
#. e.g. `dns.keys."git".hostname."github".domain."cbs.site".ownTLD = "gh"; }` 
#. configures dnsmasq so that domain "cbs.site" the fake TLD `*.gh` points to 
#. host `github.cbs.site`

# A host - referenced by a key, has (description, hostname = collection of hostRecords)
# A hostRecord - can have multiple hostname which have (domain = collection of domainRecords )
# A domainRecord - can have multiple domains which have (description, and ip4 ip6

{ config, options, lib, ... }:

with lib;

let

    hosts_options = mkOption {
        example = "poohbear";
        description = "Hostname";
        type = with types; attrsOf (submodule { options = per_key_options; });
    };

    per_key_options = { 

        description = mkOption {
            type = types.str;
            default = "";
            example = "Internal mail server for the sales dept.";
            description = ''
                Tell us about this host in this domain
            '';
        };
        
        comment = mkOption {
            type = types.str;
            default = "";
            example = "Internal mail server for the sales dept.";
            description = ''
                Tell us about this host in this domain
            '';
        };  

        ip4 = mkOption {
            type = types.str;
            default = "";
            example = "8.8.8.8";
            description = ''
                ip4Address
            '';
        };

        ip6 = mkOption {
            type = types.str;
            default = "";
            example = "it's a mysterious thing";
            description = ''
                ip6Address
            '';
        };

        address = mkOption {
            example = "mail.sales.site";
            default = {};
            description = "dnsmasq address(es)";
            type = with types; attrsOf str;
        }; 
               
        ownTLD = mkOption {
            example = "test";
            default = "";
            description = "top level domain for wildcarding";
            type = types.str;
        };   
        
        nickname = mkOption {
            example = "poohbear";
            description = "Nickname";
            default = "";
            type = types.str;
        }; 

        hostName = mkOption {
            example = "poohbear";
            default = "";
            description = "HostName";
            type = types.str;
        };

        domainName = mkOption {
            example = "poohbear.site";
            description = "domainName";
            default = "";
            type = types.str;
        };

        needsHostsEntry = mkOption {
            type = types.bool;
            default = true;
            example = true;
            description = ''
               Flag for inclusion in /etc/hosts
            '';
        };
    };
in

{
    options.dns = {
        
        domain = mkOption {
            example = "flat";
            description = "this domain";
            default = "nowhere";
            type = types.str;
        }; 

        me = mkOption {
            type = types.str;
            default = "me";
            example = "honeypot";
            description = ''
                The host table lookup 'key' for this server
            '';
        };

        hosts = mkOption {
            description = "key to the row";
            example = "server1";
            default = "me";
            type = with types; attrsOf (submodule { options = per_key_options; });
        };

        my = mkOption {
             type = with types; attrsOf str;
            default = "{}";
            example = "erm...";
            description = ''
                Our DNS info
        '';
        };

    };

    config.dns = {

    };

}
