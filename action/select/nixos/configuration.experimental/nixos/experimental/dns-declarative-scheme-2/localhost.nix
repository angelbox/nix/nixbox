# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, ... }:

{
  dns.hosts."localhost" = {
      description = "Localhost";  
      ip4 = "127.0.0.1";
      hostName = "localhost";
      ownTLD = "local";
      needsHostsEntry = false;
    };
}

