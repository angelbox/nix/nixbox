# Shell aliases etc

# The sudo NIXOS_CONFIG=$NIXOS_CONFIG construction below
# ensures that the user's definition is adopted at the sudo lovel
# (this is in preference to the more general, sudo -E option)

{ config, ... }:

{
  environment.interactiveShellInit = ''
    
    alias rebuild=/tmp/action/lib/rebuild

    function S () {
      sudo nixos-rebuild switch $@
      grep NIXOS /etc/set-environment 
      # nixos-option report | sed -n '/Value:/,/Default:/{//!p}'
    }

    function B () {
      sudo nixos-rebuild build $@
      mv -f result /tmp || true
      # nixos-option debug | sed -n '/Value:/,/Default:/{//!p}'
    }
    
    alias BT='B --show-trace'    
  '';
}
