# Shell aliases that go directly to the configuration for each box

{ ... }:

{
  environment.interactiveShellInit = ''
    alias cd_box='cd -P "''${BOX}"'
    alias cd_loc='cd -P "''${BOX}/_location_"'
    alias cd_dep='cd -P "''${BOX}/deployments"'
    alias cd_nixnet='cd -P "''${BOX}/../__"'
    alias cd_nixos='cd -P /etc/nixos'

    nix-clean () {
      nix-env --delete-generations old
      nix-store --gc
      nix-channel --update
      nix-env -u --always
      for link in /nix/var/nix/gcroots/auto/*
        do
          rm $(readlink "$link")
        done
      nix-collect-garbage -d
      nix-env -p /nix/var/nix/profiles/system --delete-generations +2
    }
  '';
}
