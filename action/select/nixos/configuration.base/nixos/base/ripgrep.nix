{ config, pkgs, ... }: {
  environment.systemPackages = [ pkgs.ripgrep ];
}
