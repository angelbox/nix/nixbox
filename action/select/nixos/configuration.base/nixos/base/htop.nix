{ config, pkgs, ... }: {
  environment.interactiveShellInit = ''
    alias htop='nix-shell -p htop --run htop'
  '';
  # environment.systemPackages = [ pkgs.htop ];
}
