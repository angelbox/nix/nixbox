# Shell aliases etc

{ config, pkgs, lib, ... }:

let 
  interface = lib.elemAt config.networking.wireless 1;
in

lib.mkIf config.networking.wireless.enable

{
 environment.systemPackages = [ 
      (pkgs.writeScriptBin "wpa_scan" (''
          #! /usr/bin/env bash

          sudo iwlist ${interface} scan | grep ESSID
      ''))  
  ];
}
