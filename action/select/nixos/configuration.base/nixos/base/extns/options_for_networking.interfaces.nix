# We want to set definitively what our useful interfaces are called
# The default so-called predictable scheme, predictably, produces 
# variable results on different hardware.

# The old-school method ought to 'predictably' use eth0 for the
# wired interface; the exception that prooves this rule turns 
# out to be the mac-mini.

# This solution creates two user definable options that we can 
# set at the box level, in order to inform other software
# definitively which interfaces are wired and which are wireless.


{ options, lib, ... }:

with lib;

{
    options.networking.wired = mkOption {
        type = with types; nullOr listOf str;
        default = [ "eth0" ];
        example = [ "enp0s10" ];
        description = "Explicitly name wired interfaces";
    };

    options.networking.wireless = mkOption {
        type = with types; nullOr listOf str;
        default = [ "wlan0" ];
        example = "wlp3s0";
        description = "Explicitly name wireless interfaces";
    };
}
