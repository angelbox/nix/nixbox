{ config, ... }:

{
  # Make surnames useful again
  environment.interactiveShellInit = ''
    umask 007
  '';
}
