{ config, pkgs, ... }:

{
 environment.systemPackages = [ 
        (pkgs.writeTextFile {
            name = "root-bashrc";
            text = ''
               cd /etc/nixos
            '';
            executable = true;
            destination = "/root/.bashrc";
        })  
  ];
}

