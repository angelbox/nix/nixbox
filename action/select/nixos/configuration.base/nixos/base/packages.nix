{ config, pkgs, ... }:

# tools that even the base box uses

{
  environment.systemPackages = with pkgs; [
    nano # editor
  ];
}