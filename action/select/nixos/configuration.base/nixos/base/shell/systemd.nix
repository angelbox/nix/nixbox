# Shell aliases etc

{ config, ... }:

{
  environment.interactiveShellInit = ''
    alias sc='sudo systemctl'
    alias jc='sudo journalctl'
  '';
}
