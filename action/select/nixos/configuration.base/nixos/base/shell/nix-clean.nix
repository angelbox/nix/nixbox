# Shell aliases that go directly to the configuration for each box

{ ... }:

{
  environment.interactiveShellInit = ''
    nix-clean () {
      nix-env --delete-generations old
      nix-store --gc
      nix-channel --update
      nix-env -u --always
      for link in /nix/var/nix/gcroots/auto/*
        do
          rm $(readlink "$link")
        done
      nix-collect-garbage -d
      nix-env -p /nix/var/nix/profiles/system --delete-generations +4
    }
  '';
}
