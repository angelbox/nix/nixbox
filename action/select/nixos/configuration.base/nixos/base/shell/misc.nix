# Shell aliases etc

{ config, ... }:

{
  environment.interactiveShellInit = ''
     
    alias  l='ls -Alh'
    alias ll='ls -l'
    alias ls='ls --color=tty'
    
    # https://opensource.com/article/19/7/bash-aliases 
    alias lt='ls --human-readable --size -1 -S --classify'
    alias gh='history|grep'
    alias left='ls -t -1'
    alias count='find . -type f | wc -l'
    alias cpv='rsync -ah --info=progress2'
    alias trash='mv --force -t ~/.local/share/Trash '
    
    function cl() {
      DIR="$*";
          # if no DIR given, go home
          if [ $# -lt 1 ]; then
                  DIR=$HOME;
      fi;
      builtin cd "$DIR" && \
          # use your preferred ls command
          ls -F --color=auto
    }
  '';
}
