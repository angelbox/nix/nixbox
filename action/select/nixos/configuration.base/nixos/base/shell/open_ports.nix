# Shell alias - list open ports

{ config, pkgs, ... }:

{
  environment.systemPackages = [ pkgs.lsof ];

  environment.interactiveShellInit = ''
    alias open_ports='lsof -i -P -n | grep LISTEN'
    alias open_connections='lsof -i -P -n | grep ESTABLISHED'
  '';
}
