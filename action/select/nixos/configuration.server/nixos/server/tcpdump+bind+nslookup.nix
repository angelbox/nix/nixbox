{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    tcpdump
    bind    # includes nslookup
  ];
}

