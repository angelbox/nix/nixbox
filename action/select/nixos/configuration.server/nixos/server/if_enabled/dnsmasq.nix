# This is a DNS server

{ config, lib, ... }:

lib.mkIf config.services.dnsmasq.enable

{ 
  services.dnsmasq = {
 
      resolveLocalQueries = lib.mkDefault true;
      extraConfig = ''
            # Do read /etc/resolv.conf
            # no-resolv - definitely not needed
            domain-needed
            bogus-priv
            log-queries
            expand-hosts
            strict-order
      '';
  };

   environment.interactiveShellInit = ''
        function dnsmasq_config()
        {
          cat $(which_dnsmasq_config)
        }
        
        function which_dnsmasq_config()
        {
          sed -nr 's/.*-C (.*\.conf).*/\1/p' /etc/systemd/system/dnsmasq.service
        }
    '';
}