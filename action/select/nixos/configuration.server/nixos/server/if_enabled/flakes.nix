{ config, pkgs, lib, ... }: {
  nix = {
    # package = pkgs.nixFlakes; # or versioned attributes like nix_2_7
    extraOptions = lib.optionalString (config.nix.package == pkgs.nixFlakes)
      "experimental-features = nix-command flakes";
   };
}
