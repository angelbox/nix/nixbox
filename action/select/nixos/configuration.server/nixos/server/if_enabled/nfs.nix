{ config, lib, ...}:

{
  # if someone enables nfs, we open the port
  networking.firewall.allowedTCPPorts = lib.mkIf config.services.nfs.server.enable [ 2049 ];
}