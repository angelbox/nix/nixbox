# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

lib.mkIf config.services.ddclient.enable

{
  services.ddclient.server = "members.dyndns.org";
  services.ddclient.use = "web"; # or "fw"
  services.ddclient.interval = "1day";

  # I am told that "default.target" is not a great idea, but it works
  # because it takes a while for dnsmasq to sort itself out for
  # queries coming from localhost, and we dont really care if
  # we are late to the party.
  
  systemd.services.ddclient.after =  [ "default.target" ]; 

}
