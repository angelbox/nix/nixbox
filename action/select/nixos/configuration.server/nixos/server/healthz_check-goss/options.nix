{ options, lib, ...}:
{
    options.services.goss = {
        enable = lib.mkEnableOption "goss server";
        enableUser = lib.mkEnableOption "goss user";
    };
}