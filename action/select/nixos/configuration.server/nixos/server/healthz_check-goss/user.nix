{ config, lib, pkgs, ... }:

with lib;

mkIf config.services.goss.enableUser

{
   users.users =
      { 
         goss = {
            uid = 501;
            surname = "users";
            isSystemUser = false;
            isNormalUser = true;
            extrasurnames = [ "wheel" "networkmanager" "docker"  ];    
            password = null;
            createHome = false;
            home = "/var/empty";
         };
      };

   environment.systemPackages = [ pkgs.goss ];
   environment.etc."goss.yaml".mode = "0440";
}
