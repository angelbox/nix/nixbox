# User config to be available on all servers

{ config, ... }:

{
  security.sudo.enable = true;
  security.sudo.wheelNeedsPassword = false;  
}