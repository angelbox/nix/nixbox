#

{ config, ... }:

{
	imports = [ ./nixos-upgrade.service.nix ];

	system.autoUpgrade.channel = https://nixos.org/channels/nixos-21.11;
}

