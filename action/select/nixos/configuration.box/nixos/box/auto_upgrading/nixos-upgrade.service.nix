{ config, ... }:

{
	system.autoUpgrade.enable = true;
	system.autoUpgrade.allowReboot = true;

	systemd.services."nixos-upgrade".environment = {
		NIXOS_CONFIG = config.environment.variables.NIXOS_CONFIG;
	};

	system.autoUpgrade.flags = [ 
		"-I" ("nixos-config=" + config.environment.variables.NIXOS_CONFIG) 
	];

}

