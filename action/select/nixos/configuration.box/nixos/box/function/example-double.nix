#. ## Adding custom functions to nix - example 1
#.
#. This is fairly easy, but not well documented.
#. To use these new functions we need to use
#. `with pkg.lib;` instead of `with lib;` since 
#. lib is defined before the overlays are applied.
#< p

{ pkgs, ... }:
{
  nixpkgs.overlays = [

    ( final: prev: { 
        
        lib = (prev.lib or {}) //  { 
                                    double = n: 2 * n; 
                                   };  
        }
    )
   ];
}

