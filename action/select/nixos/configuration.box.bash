# box

me="${BASH_SOURCE[0]}"
here="${me%/*}"
this_configuration="${me%.bash}"
this_option="${this_configuration##*/}"
requested_configuration="${requested_configuration:-$this_option}"
nixos="${nixos:-/etc/nixos}"

rsync -a --delete "${this_configuration}/nixos/"* "$nixos"

# configuration-box.nix is uploaded by unlock

"$here/../../remote/set_configuration" "$requested_configuration"

