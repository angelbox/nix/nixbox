# box orig-inal

nixos="${nixos:-/etc/nixos}"

rm "$nixos/configuration.nix"
ln -s "configuration.orig.nix" "$nixos/configuration.nix"

echo "Switched configuration.nix -> configuration.orig.nix"