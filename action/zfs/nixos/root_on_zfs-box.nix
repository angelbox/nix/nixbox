# +action/zfs/format/basic-boot.nix
# filesystem.nix

{ config, pkgs, lib, ... }:

{  
  networking.hostId = builtins.substring 0 8 (builtins.readFile "/etc/machine-id");

  # Select the kernal version (must support OpenZFS)
  boot.kernelPackages = lib.mkDefault pkgs.linuxPackages_5_15;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.timeout = 3;
  
  # grub2 boot loader options
  boot.loader.grub.efiSupport = true;

  # for ssd
  services.zfs.autoScrub.enable = true;

  services.zfs.autoScrub.pools = [ "POOL" ];

  fileSystems."/" =
    { device = "none";
      fsType = "tmpfs";
      options = [ "size=2G" "mode=755" ]; # mode=755 so only root can write to those files
    };

  fileSystems."/etc" =
    { device = "POOL/os/etc";
      options = [ "zfsutil" ];
      fsType = "zfs";
    };

  fileSystems."/nix" =
    { device = "POOL/os/nix";
      options = [ "zfsutil" ];
      fsType = "zfs";
    };

  fileSystems."/var" =
    { device = "POOL/os/var";
      options = [ "zfsutil" ];
      fsType = "zfs";
    };

  fileSystems."/var/spool" =
    { device = "POOL/os/var/spool";
      options = [ "zfsutil" ];
      fsType = "zfs";
    };

  fileSystems."/var/log" =
    { device = "POOL/os/var/log";
      options = [ "zfsutil" ];
      fsType = "zfs";
    };

  fileSystems."/var/lib" =
    { device = "POOL/os/var/lib";
      options = [ "zfsutil" ];
      fsType = "zfs";
    };

  fileSystems."/boot" =
    { 
      device = "/dev/sda2";
      # device = "/dev/disk/by-label/EFI";

      fsType = "vfat";
      options = [ "X-mount.mkdir" ];
    };

  # automount facility

  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.devNodes = "/dev/disk/by-path";
  
  # http://toxicfrog.github.io/automounting-zfs-on-nixos/
  boot.postBootCommands = ''
 
        # Mount all the other zfs volumes automatically
        # to save having to manage them explicitly above

	echo "=== STARTING ZPOOL IMPORT ==="

	# Clean up borg-repo because the activation script creates it
	# KPH - not using borg so commenting out this line
	# ${pkgs.findutils}/bin/find /backup/borg-repo -maxdepth 1 -type d -empty -delete

	${pkgs.zfs}/bin/zpool import -a -f -N -d /dev/disk/by-path
	${pkgs.zfs}/bin/zpool status
 	${pkgs.zfs}/bin/zfs mount -a

	echo "=== ZPOOL IMPORT COMPLETE ==="
  
 '';

  environment.interactiveShellInit = ''
    alias zfs='sudo zfs'
    alias zl='zfs list'
    alias zh='zpool history'
    alias zs='zpool status'
  '';
}
