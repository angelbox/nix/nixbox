#!/usr/bin/env bash

# parameters:
# 1) a calling script can simply provide '$me' in order to change the format &/ template
# 2) $pool/$POOL can be set via the environment or /boot/box.env

set -aeuo pipefail
$DEBUG && set -x

# me = <here> / <template>          .bash
# me = <here> / <format> - <option> .bash

here="${BASH_SOURCE[0]%/*}"
me="${me:-${BASH_SOURCE[0]}}" # caller can provide '$me'
template="${me%.bash}"
template="${template##*/}"
format="${template%%-*}"
option="${template#*-}"


# the simplest template processoser ever
function render_template()
{
  eval "echo \"$(cat $1)\""
}

if [[ ! -d /boot ]]; then
  mkdir -p /boot
  mount /dev/${DEVICE:-sda}2 /boot
  echo "Mounted /dev/${DEVICE:-sda}2 /boot"
fi 

source "$here"/../../lib/properties_read /boot/box.env

[[ -z ${pool+x} ]] && [[ -z ${POOL+x} ]] && pool=C1
POOL="${POOL:-$pool}"

# format zfs pool if not present
if [[ $(zpool import 2>&1) == *"no pools available"* ]]; then
  "$here/${format}.format"
else
  zpool import $POOL
fi 

[[ -z ${hostname+x} ]] && echo "\$hostname not set" && exit 1
echo "Using hostname=$hostname"

mkdir -p /mnt/etc/nixos

sed "s/POOL/${POOL}/g" "$here/${format}-box.nix" > /mnt/etc/nixos/configuration-box.nix
cat "${here}/../../hw/${venue}_${shape}-hw.nix"  > /mnt/etc/nixos/configuration-hw.nix

# /boot is already mounted, use a bind mount
[[ -d /mnt/boot ]] || mount -m --bind /boot /mnt/boot

nixos-generate-config --root /mnt

if [[ ! -f "/mnt/etc/nixos/configuration.orig.nix" ]]; then
  mv "/mnt/etc/nixos/configuration.nix" "/mnt/etc/nixos/configuration.orig.nix" 
fi

render_template "$here/../../config/nixos/box-${option}.configuration.tmpl" > /mnt/etc/nixos/configuration.${option}.nix

[[ -f /mnt/etc/nixos/configuration.nix ]] && rm /mnt/etc/nixos/configuration.nix

ln -s configuration.${option}.nix /mnt/etc/nixos/configuration.nix

echo "Ready to install via: ssh ${1%/*}/install"