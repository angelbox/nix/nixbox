#!/usr/bin/env bash

[[ "${CONFIRM:-}" != "yes" ]] && echo "Dry run, $1:CONFIRM=yes" && exit 1

nixos-install --show-trace --root /mnt --no-root-password

