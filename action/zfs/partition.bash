#!/bin/sh

[[ "${CONFIRM:-no}" != "yes" ]] && echo "This is destructive, to proceed $1:CONFIRM=yes" && exit 1

DEVICE="${DEVICE:-sda}"

[[ $(hostname) != "kexec" ]] && echo "Not in kexec-ramboot mode" && exit

parted --script --align minimal "/dev/$DEVICE" -- \
    mklabel gpt \
    mkpart primary 300MiB -0 \
    mkpart ESP fat32 0 300MiB \
    set 2 esp on

mkfs.fat -F 32 -n EFI "/dev/${DEVICE}2"

fdisk -l

mkdir -p /boot
mount /dev/${DEVICE}2 /boot
echo "Mounted /dev/${DEVICE}2 /boot"