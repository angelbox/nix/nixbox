
DEBUG=${DEBUG:-false}
VERBOSE=${VERBOSE:-true}
QUIET=${QUIET:-false}

# Terminal colour
green () { printf "\e[1m\e[32m" ; "$@" ; printf "\e[0m"; }
red    () { printf "\e[1m\e[31m" ; "$@" ; printf "\e[0m"; }
cyan   () { printf "\e[1m\e[36m" && "$@" && printf "\e[0m" ; }
yellow () { printf "\e[1m\e[33m" && "$@" && printf "\e[0m" ; }

# Volume modifiers (specify preferred output from a command)
loud   () { $QUIET && { "$@" &2>1 > /dev/null ; } || "$@" ;}
shh    () { $VERBOSE && "$@" || "$@" &2>1 > /dev/null ; }
shhd   () { $DEBUG && "$@" || "$@" &2>1 > /dev/null; }

show ()
{
    for v in $@; do
        printf "%s: %s\n" "${v^}" "${!v}"
    done
}

name_surname ()
{
    local name_surname="${1%%+*}"     # remove action suffix (longest trailing +portion)
    name_surname="${name_surname#*^}" # remove prefix re.set^ prep^ etc.
    name=${name_surname%.*}           # remove the surname portion until after last $.
    surname="${name_surname##*.}"     # find the final surname portion as the configuration family
}

host_path ()
{
    host="${1%%:*}"
    path="${1#*:}"   
}

# Find config in users .ssh/*/.conf, via a standard search path
find_box_config ()
{
    box_config=$(find ~/.ssh -type d \( \
        -path "*/$name.$surname/box.env"
        \) -print -quit)

    box_config="${box_config%/box.env}"
}

# Potential Properties
property_defaults ()
{
    nixos="${nixos:-/etc/nixos}"
}