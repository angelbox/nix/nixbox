#!/usr/bin/env bash

set -uo pipefail

# use lengths wherever possible - faster
function split_byStr_bash_faster()
{
    array=()
    local car=""
    local cdr="$2"
    while
        car="${cdr%%"$1"*}"
        array+=("$car")
        cdr="${cdr:${#car}}"
        (( ${#cdr} ))
    do
        cdr="${cdr:${#1}}"
    done
}

function url_decode() { : "${*//+/ }"; echo -e "${_//%/\\x}"; }

# separator_hunt returns: 
# 1) $action (up to found separator)
# 2) $params (the params data portion)
# 2) $sep    (found separator, or empty)

function separator_hunt()
{
    local all="$1"

    for sep in ',' ':'
    do

        action="${all%%$sep*}" # get up to the first separator candidate
        params="${all#*$sep}"  # get after the first separator candidate

        case "$action" in 
            *[:,]* ) continue # class cannot contain other separator - next
            ;;
        esac

        if [[ "$action" != "$all" ]]; then # no separator found
            break
        fi

       sep=''
       
    done
}

function into_vars()
{
   printf -v "$1" "%s" "$(url_decode "${2}")"
}

# read %n from ssh parse trailing portion into vaiables
function read_params ()
{
    local set_fn="$1" get_fn="$2"
    separator_hunt "${param#*+}" # remove up to the first $+  

    local kv key value array

    if [[ -n "$sep" ]]; then

        split_byStr_bash_faster "$sep" "${params}"

        # $kv = <key>=<value>
         
        for kv in "${array[@]}"
        do
            key="${kv%%=*}" # get up to the first $=

            if [[ "$kv" == "$key" ]]; then # no value
                $get_fn "$key"
            else 
                value="${kv#*=}" # remove up to the first $=
                $set_fn "$key" "$value"
            fi 
        done
    fi
}

function read_hostname_surname() 
{
    # remove prefix portion
    param="${1##*^}"

    # we pick off the <host>.<surname> from the original request
    host_dot_arena="${param%%+*}"  # get up to first $+ (after removing prefix)
    hostname="${host_dot_arena%%.*}"     # get up to first $.
    surname="${host_dot_arena#*.}"         # get after first dot, the rest
}