## NixBox™ - Server Superpowers

Welcome to NixBox™, a Ssh-wish-army-knife designed to get NixOS servers up and running whatever the cloud. NixBox itself is as lightweight as possible, as it stands it does not impose any structure or scheme on the subsequent configuration.

NixBox is intended to provide a hardware/cloud independant substrate for NixNet, but it can be used standalone.

Full blown frameworks tend to increase the learning curve; the aim here is to provide an approach to running a suite of servers, that is more assisted, than automated.

### Ssh-wish actions

Some useful hacks are built in, infect any remote server!

```
$> This is a hostile takeover!
$> ssh ubuntu@prep.demo-1.site.nixos-infect.confirm.yes
```

`kexec-ramboot` is useful for reformatting a host's underlying block-storage; ramboot any remote server that has nix installed.

```
$> ssh prep.demo-1.site.ramboot
```

### ssh++ prep.\*

The universal prefix, `prep.*`disables host key checking until your server is fully ready and has its host keys set correctly.

### ssh++ \*.set & \*.get

Define properties on any instance, to help with self-reflection.

```
$> ssh demo-1.site.set.cloud:oci
$> ssh demo-1.site.set.region:uk-london-1
$> ssh demo-1.site.set.hw:VM.Standard.E2.1.Micro

$> ssh demo-1.site.get
cloud=oci
region=uk-london-1
hw=VM.Standard.E2.1.Micro
```

### ssh++ \*.unlock & \*.lock

The ability to maintain secrets off-server; upload them just prior to rebuilding/updating.

### ssh++*\*.build & \**.switch

Remote control of common `nixos-rebuild` actions

### ssh++ combinations

```
$> ssh demo-1.site.unlock.switch          # ..or.. ssh demo-1.site.U.S
```

### ssh++ configure the box

```
$> ssh demo-1.site.configure.nixbox:bare           # ..or..
$> ssh demo-1.site.configure.nixbox:server         # ..or..
$> ssh demo-1.site.configure.nixbox:nas            # ..or..
```

### ssh++ application/service templates

Supporting the [NMMM](https://jappie.me/the-nix-mutli-monolith-machine-nmmm.html) paradigm; where possible configuration templates mix and match to provide a composable system.

```
$> ssh demo-1.site.configure.app:honeypot         # ..and/or..
$> ssh demo-1.site.configure.app:nameserver       # ..and/or..
$> ssh demo-1.site.configure.app:wireguard-ap     # ..and/or..
$> ssh demo-1.site.configure.app:nextcloud        # ..requires..
$> ssh demo-1.site.configure.app:nextcloud-mysql  # ..or..
$> ssh demo-1.site.configure.app:nextcloud-pg     # ...
```

### ssh++ \*.clean 

tidy up.
