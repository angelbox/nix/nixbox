#!/usr/bin/env bash

DIR="${BASH_SOURCE%/*}"
source "$DIR/../specs/bash-spec.sh"

source "$DIR/shared.sh"

describe "The lib/params parser sets environment variables" && {

  it "using , separator" && {
    read_params into_vars : "^server.site+set,A=test,BE=a:treat,cee=say%20my%20name"
    
    expect_var A   to_be test
    expect_var BE  to_be "a:treat"
    expect_var cee to_be "say my name" 

  }

  it "using : separator" && {
    read_params into_vars : "^server.site+set:A=test:BE=a,treat:cee=my%20name%20say"
    
    expect_var A   to_be test
    expect_var BE  to_be "a,treat"
    expect_var cee to_be "my name say" 

  }
}

