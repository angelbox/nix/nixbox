#!/usr/bin/env bash

DIR="${BASH_SOURCE%/*}"
source "$DIR/../specs/bash-spec.sh"

file=$(mktemp -u)
rm -f /tmp/no_file 2>&1 > /dev/null

describe "The properties are persisted in a file" && {

  context "When the file does not exist" && {

    it "+action - show - returns nothing" && {
      
      out=$( $DIR/property test.site+box /tmp/no_file )
      expect_var out to_be ""
      expect /tmp/no_file not to_exist
    }

     it "+action:key - get - returns nothing" && {
      
      out=$( $DIR/property test.site+box:key /tmp/no_file )
      expect_var out to_be ""
      expect /tmp/no_file not to_exist
    }

    it "+action:key=value - put - creates file" && {
      
      expect $file not to_exist
      out=$( $DIR/property test.site+box:key=2 $file )
      expect_var out to_be ""
      expect $file to_exist    

      content=$(<$file)
      expect_var content to_be "key='2'"
    }

    it "+action:key - get - returns value" && {
 
      value=$( $DIR/property test.site+box:key $file )
      expect_var value to_be "2"
    }
  }
 
     it "+action:key=value - put - additional row" && {
      
      out=$( $DIR/property test.site+box:hope=true:fear=false,or,bad $file )
      expect_var out to_be ""
      expect $file to_exist    

      content=$(<$file)
      expect_var content to_be "key='2'" "hope='true'" "fear='false,or,bad'"
    }

    it "+action - show display whole file" && {
      
      content=$( $DIR/property test.site+box $file )
      expect_var content to_be "key='2'" "hope='true'" "fear='false,or,bad'"
    }

    it "properties.read - reads into variables" && {
      
      source $DIR/properties_read $file

      expect_var key to_be 2
      expect_var hope to_be true
      expect_var fear to_be 'false,or,bad'
    }

    cat $file

    it "property  test.site+box,hope=,key=,fear= - empty value removes key" && {
            
      out=$( $DIR/property test.site+box,hope=,key=,fear= $file )
 
      out=$( $DIR/property test.site+box $file )

      expect_var out to_be ''
    }

    it "property  test.site+box:hope=a+b+c-d" && {
            
      out=$( $DIR/property test.site+box:hope=a+b+c-d $file )
 
      source $DIR/properties_read $file

      expect_var key to_be 2
      expect_var hope to_be 'a b c-d'

    }

  rm $file
}
