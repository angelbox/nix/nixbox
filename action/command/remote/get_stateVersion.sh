#!/usr/bin/env bash

here="${BASH_SOURCE%/*}"
source "$here/shared.sh"

source "$here"/properties_read "$3" # may override surname/hostname/nixos

[[ -n "${stateVersion}" ]] && echo "${stateVersion}" && exit 0

STATEVERSION=$(sed -n 's/.*stateVersion.*\"\(.*\)\".*/\1/p' /etc/nixos/configuration.nix)

echo "${STATEVERSION:-$1}"