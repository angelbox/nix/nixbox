#!/usr/bin/env bash

# A basic script for running all the matching executable scripts in the folder
# Success/Failure is reported

cd "${BASH_SOURCE[0]%/*}/.."      

echo "Working Directory:" $(pwd)

DEBUG=${DEBUG:-false}
LOUD=${LOUD:-false}

$DEBUG && echo "Which bash" $(which bash)
$DEBUG && find . -name "[^_]*.spec"

set +e # do not stop on fail
fails=0
for test in $(find . -name "[^_]*.spec");
do
   if [[ -x "$test" ]]; then
       printf ">$test "
       out=$("$test")
       [[ $? != 0 ]] && fails=$((fails + 1)) && echo "X" || echo "√"
       $LOUD && echo "$out"
   fi
done

[[ $fails == 0 ]] && echo "Pass" || echo "Fails: $fails"

exit $fails
