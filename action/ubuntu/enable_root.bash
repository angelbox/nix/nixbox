#!/usr/bin/env bash

[[ !  -d "/etc/nixos" ]] && sudo mkdir -p /etc/nixos 

[[ ! -f /home/ubuntu/.ssh/authorized_keys ]] && echo "no keys available for ubuntu user" && exit 

echo "Copying authorized_keys from user ubuntu to root:"

sudo cp -v /root/.ssh/authorized_keys /root/.ssh/authorized_keys.bk
sudo cp -v /home/ubuntu/.ssh/authorized_keys /root/.ssh/authorized_keys
