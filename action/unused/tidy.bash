#!/usr/bin/env bash
# run as sudo or owner account

set -euo pipefail
set -x

DEST="$1"

# Not sure whether the -cnewer clause will work as intended

find "$DEST" \
    ! -cnewer "$DEST"/. \
    ! -path "$DEST/configuration*.nix" \
    ! -path "$DEST/*-configuration.nix" \
    -delete
