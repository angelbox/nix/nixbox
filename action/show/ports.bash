#!/usr/bin/env bash

if [[ -n $(which lsof 2> /dev/null) ]] 
then
  lsof -i -P -n | grep LISTEN
else
  nix-shell -p lsof --run "lsof -i -P -n | grep LISTEN" 2> /dev/null && exit
fi

sudo netstat -tulpn | grep LISTEN