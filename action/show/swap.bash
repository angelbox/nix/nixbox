#!/usr/bin/env bash

swapon

echo

printf "vm.swappiness=%s\n" $(cat /proc/sys/vm/swappiness)
printf "vm.vfs_cache_pressure=%s\n" $(cat /proc/sys/vm/vfs_cache_pressure)
printf "vm.dirty_background_ratio=%s\n" $(cat /proc/sys/vm/dirty_background_ratio)
printf "vm.dirty_ratio=%s\n" $(cat /proc/sys/vm/dirty_ratio)
