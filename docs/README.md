# Nix Box

### Layering

The most important principle in NixBox is that of layering, such that
NixBox itself forms a self-sufficent and complete layer of abstraction
upon which to build and deploy servers.

Within NixBox the layering principle is repeatedly applied in order that
layers can be pared back for debugging purposes.

#### Original

The `original`-layer, accessible via `ssh <server>+action/config/nixos/box-original`, uses the original configuration files provided on the box 
by the original installation process, without any nixbox embellishments.

#### Base

The `base`-configuration, accessible via `ssh <server>+action/config/nixos/box-base`, has serveral layers. The first is a translation of the original configuration into nixbox idioms, adopting snippets of common code,
via the ./hw/... e.g. `./hw/boot/systemd-efi.nix`

The second layer introduces a function that imports a whole folder of nix files.
The `./base` folder contents provide the basis of nixbox, the abstraction upon which to build servers.


## More

resources for further development
* https://www.reddit.com/r/NixOS/comments/zo8acc/learning_nixos_as_a_newbie/
* https://blog.project-insanity.org/2022/12/22/nextcloud-development-environment-on-nixos/
* https://lucperkins.dev
* https://github.com/the-nix-way/dev-templates

## MacOS X / Dependencies

Installed from nixpkgs:
nix-env -iA nixpkgs.bashInteractive
nix-env -iA nixpkgs.mktemp
nix-env -iA nixpkgs.gnused
nix-env -iA nixpkgs.makeself